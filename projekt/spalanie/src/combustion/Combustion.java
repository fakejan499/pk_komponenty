/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package combustion;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;

/**
 * @author fakejan
 */
public class Combustion extends JLabel implements java.io.Serializable {
    private List<List<Float>> sections = new ArrayList(new ArrayList());
    private List<Float> currentSection = null;
    private float okAvgBellow = 8;
    private Color okColor = Color.GREEN;
    private Color badColor = Color.RED;

    private float startDistance;
    private float endDistance;

    @Override
    public String toString() {
        return "Combustion{" + "sections=" + sections + ", currentSection=" + currentSection + ", okAvgBellow=" + okAvgBellow + ", okColor=" + okColor + ", badColor=" + badColor + ", startDistance=" + startDistance + ", endDistance=" + endDistance + '}';
    }

    public List<List<Float>> getSections() {
        return sections;
    }

    public void setSections(List<List<Float>> sections) {
        this.sections = sections;
    }

    public float getStartDistance() {
        return startDistance;
    }

    public void setStartDistance(float startDistance) {
        this.startDistance = startDistance;
    }

    public float getEndDistance() {
        return endDistance;
    }

    public void setEndDistance(float endDistance) {
        this.endDistance = endDistance;
    }

    public List<Float> getCurrentSection() {
        return currentSection;
    }

    public void setCurrentSection(List<Float> currentSection) {
        this.currentSection = currentSection;
    }

    public float getOkAvgBellow() {
        return okAvgBellow;
    }

    public void setOkAvgBellow(float okAvgBellow) {
        this.okAvgBellow = okAvgBellow;
    }

    public Component getLabelFor() {
        return labelFor;
    }

    public void setLabelFor(Component labelFor) {
        this.labelFor = labelFor;
    }

    public ComponentUI getUi() {
        return ui;
    }

    public void setUi(ComponentUI ui) {
        this.ui = ui;
    }

    public EventListenerList getListenerList() {
        return listenerList;
    }

    public void setListenerList(EventListenerList listenerList) {
        this.listenerList = listenerList;
    }

    public AccessibleContext getAccessibleContext() {
        return accessibleContext;
    }

    public void setAccessibleContext(AccessibleContext accessibleContext) {
        this.accessibleContext = accessibleContext;
    }

    public Combustion() {
        setOpaque(false);
    }

    @Override
    public void paint(Graphics g) {
        if (isOk()) g.setColor(okColor);
        else g.setColor(badColor);
        g.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, getWidth() - 1, getHeight() - 1);
        super.paint(g); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean isOk() {
        return getAvgOverDistance() < okAvgBellow;
    }

    public void start(float distance) {
        if (currentSection == null) {
            currentSection = new ArrayList();
            startDistance = endDistance = distance;
        } else throw new RuntimeException("Wywołaj \"stop\" zanim zaczniesz nowy odcinek!");
        repaint();
    }

    public int stop() {
        if (currentSection == null)
            throw new RuntimeException("Nie można zakończyć odcinka, jeśli nie został on wcześniej rozpoczęty");

        sections.add(currentSection);
        currentSection = null;
        repaint();

        return sections.size() - 1;
    }

    public void addData(float distance, float comsumption) {
        if (currentSection == null)
            throw new RuntimeException("Nie można zdodać danych, jeśli odcinek nie został wcześniej rozpoczęty");

        if (distance <= endDistance) throw new RuntimeException("Niepoprawny wskaźnik dystansu");
        if (comsumption < 0) throw new RuntimeException("Niepoprawna wartość spalania");

        for (int i = 0; i < distance - endDistance; i++) {
            currentSection.add(comsumption);
        }

        endDistance = distance;
        repaint();
    }

    public float getAvgOverDistance() {
        float distance = 0;
        float combustion = 0;
        for (List<Float> section : sections) {
            distance += section.size();

            combustion += getCombustion(section);
        }
        return combustion / distance;
    }

    public float getAvgOverDistance(int id) {
        List<Float> s = sections.get(id);
        return getCombustion(s) / s.size();
    }

    public float getCombustion(List<Float> combustions) {
        float combustion = 0;
        for (Float c : combustions) {
            combustion += c;
        }
        return combustion;
    }

    public void save() {
        try {
            serializeToXML();
            repaint();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void serializeToXML() throws IOException {
        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream("settings.xml"));
        XMLEncoder encoder = new XMLEncoder(fos);
        encoder.setExceptionListener(new ExceptionListener() {
            public void exceptionThrown(Exception e) {
                System.out.println("Exception! :" + e.toString());
            }
        });
        encoder.writeObject(this);
        encoder.close();
        fos.close();
    }

    public void restore() {
        try {
            restoreState(deserializeFromXML());
            repaint();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private Combustion deserializeFromXML() throws IOException {
        BufferedInputStream fis = new BufferedInputStream(new FileInputStream("settings.xml"));
        XMLDecoder decoder = new XMLDecoder(fis);
        Combustion c = (Combustion) decoder.readObject();
        decoder.close();
        fis.close();
        return c;
    }

    private void restoreState(Combustion c) {
        sections = c.sections;
        currentSection = c.currentSection;
        okAvgBellow = c.okAvgBellow;
        okColor = c.okColor;
        badColor = c.badColor;

        startDistance = c.startDistance;
        endDistance = c.endDistance;

    }

    public int sections() {
        return sections.size();
    }

    public void setOkAvgCombustion(float arg) {
        okAvgBellow = arg;
        repaint();
    }

    public void setOkColor(Color c) {
        okColor = c;
        repaint();
    }

    public void setBadColor(Color c) {
        badColor = c;
        repaint();
    }
} 
