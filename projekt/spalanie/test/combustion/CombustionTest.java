/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package combustion;

import java.awt.Color;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertFalse;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author fakejan
 */
public class CombustionTest {
    private Combustion cut;
    
    @Before
    public void setUp() {
        cut = new Combustion();
    }
    
    @Test
    public void oneSection() {
        cut.start(0);
        cut.addData(5, 8);
        cut.addData(10, 8);
        cut.addData(99, 8);
        cut.stop();
        
        assertEquals(8f, cut.getAvgOverDistance());
    }
    
    @Test
    public void multipleSections() {
        cut.start(0);
        cut.addData(5, 8);
        cut.addData(10, 8);
        cut.addData(15, 8);
        cut.stop();
        
        cut.start(0);
        cut.addData(5, 10);
        cut.addData(10, 10);
        cut.addData(15, 10);
        cut.stop();
        
        assertEquals(9f, cut.getAvgOverDistance());
    }
    
    @Test 
    public void avgCombustionOnSection() {
        cut.start(0);
        cut.addData(5, 8);
        cut.addData(10, 8);
        cut.addData(15, 8);
        cut.stop();
        
        cut.start(0);
        cut.addData(5, 10);
        cut.addData(10, 10);
        cut.addData(15, 10);
        cut.stop();
        
        assertEquals(10f, cut.getAvgOverDistance(1));
    }
    
    @Test
    public void isOk() {
        cut.start(0);
        cut.addData(5, 9);
        cut.stop();
        
        cut.setOkAvgCombustion(11);
        
        assertTrue(cut.isOk());
        
        cut.setOkAvgCombustion(8.5f);
        
        assertFalse(cut.isOk());
    }
}
