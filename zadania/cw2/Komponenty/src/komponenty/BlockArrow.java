/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponenty;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

/**
 *
 * @author fakejan
 */
public class BlockArrow extends Component {
    private ArrowDirection direction = ArrowDirection.RIGHT;

    public BlockArrow() {
        setPreferredSize(new Dimension(30, 30));
        setForeground(Color.CYAN);
    }

    public ArrowDirection getDirection() {
        return direction;
    }

    public void setDirection(ArrowDirection direction) {
        firePropertyChange("direction", this.direction, direction);
        this.direction = direction;
    }
    
    
    public synchronized void paint(Graphics g) {
     Dimension d = getSize();
   int w = d.width-1;
   int h = d.height-1;   
   int[] xPoints = {0,0,w};     // default direction is RIGHT
   int[] yPoints = {h,0,h/2};
   switch (direction) {
      case LEFT: xPoints = new int[]{w,w,0};  // yPoints - no change
                 break;
      case UP:   xPoints = new int[]{w,0,w/2};
                 yPoints = new int[]{h,h,0};
                 break;
      case DOWN: xPoints = new int[]{w,0,w/2};
    }
    
    g.drawLine(xPoints[0], yPoints[0], xPoints[1], yPoints[1]);
    g.drawLine(xPoints[1], yPoints[1], xPoints[2], yPoints[2]);
    g.drawLine(xPoints[2], yPoints[2], xPoints[0], yPoints[0]);
    }
}
